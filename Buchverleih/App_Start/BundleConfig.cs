﻿using System.Web;
using System.Web.Optimization;

namespace Buchverleih
{
    public class BundleConfig
    {
        // Weitere Informationen zur Bündelung finden Sie unter https://go.microsoft.com/fwlink/?LinkId=301862.
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Script-Bundles ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            bundles.Add(new ScriptBundle("~/bundles/Scripts/jquery")
                .Include(
                        "~/Scripts/jquery/jquery-{version}.js",
                        "~/Scripts/jquery/jquery-{version}.min.js",
                        "~/Scripts/jquery/jquery-{version}.intellisense.js",
                        "~/Scripts/jquery/jquery-{version}.slim.js",
                        "~/Scripts/jquery/jquery-{version}.slim.min.js",
                        "~/Scripts/jquery/jquery.validate.js",
                        "~/Scripts/jquery/jquery.validate-min.js",
                        "~/Scripts/jquery/jquery.validate-vsdoc.js",
                        "~/Scripts/jquery/jquery.validate-unobtrusive.js",
                        "~/Scripts/jquery/jquery.validate-unobtrusive-min.js",
                        "~/Scripts/jquery/jquery.min.js"));
                       
            /* ======================================== Plugins ========================================*/
            bundles.Add(new ScriptBundle("~/bundles/plugins")
                .Include(
                       "~/Scripts/plugins/slick.js",
                       "~/Scripts/plugins/slick.min.js",
                       "~/Scripts/plugins/script.js",
                       "~/Scripts/plugins/DatePickerX.js",
                       "~/Scripts/plugins/DatePickerX.min.js"));

            /* ======================================== Modernizer ========================================*/
            bundles.Add(new ScriptBundle("~/bundles/modernizr")
                .Include(
                       "~/Scripts/modernizer/modernizr-{version}.js"));

            /* ======================================== Bootstrap ========================================*/
            bundles.Add(new ScriptBundle("~/bundles/bootstrap")
                .Include(
                      "~/Scripts/bootstrap/bootstrap.js",
                      "~/Scripts/bootstrap/bootstrap.min.js",
                      "~/Scripts/bootstrap/bootstrap-switch.js",
                      "~/Scripts/bootstrap/bootstrap.bundle.min.js"));

            /* ======================================== Custom ========================================*/
            bundles.Add(new ScriptBundle("~/bundles/custom")
               .Include(
                       "~/Scripts/custom/filterList.js"));


           // Verwenden Sie die Entwicklungsversion von Modernizr zum Entwickeln und Erweitern Ihrer Kenntnisse. Wenn Sie dann
           // bereit ist für die Produktion, verwenden Sie das Buildtool unter https://modernizr.com, um nur die benötigten Tests auszuwählen.


            #endregion

            #region ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Style-Bundles ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~              

           /* ======================================== Bootstrap ========================================*/
           bundles.Add(new StyleBundle("~/Content/bootstrap")
                .Include(
                      "~/Content/bootstrap/bootstrap.css",
                      "~/Content/bootstrap/bootstrap.min.css",
                      "~/Content/bootstrap/bootstrap-theme.css",
                      "~/Content/bootstrap/bootstrap-switch.css",
                      "~/Content/bootstrap/Site.css")
                .IncludeDirectory("~/Content/bootstrap", "*.css"));

            /* ======================================== Footer ========================================*/
            bundles.Add(new StyleBundle("~/Content/footer")
              .Include(
                 "~/Content/footer/footer.css",
                 "~/Content/footer/fontawesome.css",
                 "~/Content/footer/fontawesome.min.css")        //TODO - Daran arbeiten
                 .IncludeDirectory("~/Content/footer","*.css"));

            /* ======================================== Plugins ========================================*/
            bundles.Add(new StyleBundle("~/Content/plugins")
                  .Include(
                        "~/Content/plugins/slick.css",
                        "~/Content/plugins/slick-theme.css",                         
                          "~/Content/plugins/responsive.css",
                             "~/Content/plugins/uikit.css",
                             "~/Content/plugins/DatePickerX.css",
                             "~/Content/plugins/DatePickerX.min.css"));

            /* ======================================== Custom ========================================*/
            bundles.Add(new StyleBundle("~/Content/custom")
               .Include(
                       "~/Content/custom/listStyle.css"));
            #endregion
        }
    }
}
