﻿using Buchverleih.DAL.Data;
using Buchverleih.DAL.Logic;
using Buchverleih.Models;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

using Buchverleih.Hilfsklassen;

namespace Buchverleih.Controllers
{
    //TODO - Ausleihdatum festlegen
    //TODO - Bücher nach Ablauf der Ausleihfrist (+7 Tage) wieder verfügbar
    public class BuchController : Controller
    {
        [HttpGet]
        public ActionResult Buchliste()
        {            
            List<BuchVM> vmBuecher = new List<BuchVM>();
            foreach (Buch Buch in BuchManager.LadeBuecher())
            {
                //Für jedes Buch aus der DB ein neues BuchVM Objekt erzeugen 
                BuchVM vmBuch = new BuchVM();
                //Für jedes Buch Mapping von Buch zu BuchVM
                MappeBuchZuBuchVM(vmBuch, Buch);
                //BuchVM der Liste mit VMBuch hinzufügen
                vmBuecher.Add(vmBuch);
            }

            return View(vmBuecher);
        }

        public ActionResult Katliste(string x)
        {
            TempData["Filter"] = x;

            return RedirectToAction("Buchliste");
        }

        [HttpGet]
        public ActionResult Detail(int buchId)
        {
            BuchDetailVM vmBuchDetail = new BuchDetailVM();

            //BUch-Daten aus der DB laden
            Buch dbBuch = BuchManager.LadeBuch(buchId);

            if (dbBuch == null)
            {
                return RedirectToAction("Buchliste", "Buch");
            }

            MappeBuchZuBuchDetailVM(vmBuchDetail, dbBuch);

            return View(vmBuchDetail);
        }

        #region Mapping-Methoden
        private static void MappeBuchZuBuchDetailVM(BuchDetailVM vmBuchDetail, Buch dbBuch)
        {
            //Ergebnisse von dbBuch in vmBuchDetail mappen
            vmBuchDetail.BuchId = dbBuch.BuchId;
            vmBuchDetail.Anzahl = dbBuch.Anzahl;
            vmBuchDetail.Ausgabe = dbBuch.Ausgabe;
            vmBuchDetail.BildAltText = dbBuch.BildAltText.BildTextAufbereitung();
            vmBuchDetail.Bildpfad = dbBuch.Bildpfad.BildPfadAufbereitung();
            vmBuchDetail.BuchBeschreibung = dbBuch.BuchBeschreibung;
            vmBuchDetail.Erscheinungsjahr = dbBuch.Erscheinungsjahr;
            vmBuchDetail.ISBN = BuchManager.FormatiereISBN(dbBuch.ISBN);
            vmBuchDetail.KatBezeichnung = dbBuch.Kategorie.KatBezeichnung;
            vmBuchDetail.PreisProTagBrutto = Convert.ToDouble(dbBuch.PreisProTagBrutto);
            vmBuchDetail.Titel = dbBuch.Titel;
            
            /*TODO - Logik für Reservierung / ReservierungsManager bauen
            if (dbBuch.Verfügbarkeit <0)
            {
                vmBuchDetail.VerfügbarAb = dbBuch.Reservierungszeile.verliehenbis + 1;
            }            
            */
            vmBuchDetail.VerfügbarAb = DateTime.Today.ToShortDateString();
            vmBuchDetail.Verfügbarkeit = dbBuch.Verfügbarkeit;
            vmBuchDetail.VerlagBezeichnung = dbBuch.Verlag.VerlagBezeichnung;
            vmBuchDetail.VerleihHäufigkeit = dbBuch.VerleihHäufigkeit;
            vmBuchDetail.Autoren = new List<string>();

            foreach (BuchAutor dbBuchAutor in dbBuch.BuchAutor)
            {
                string autorname = BuchManager.LadeAutorname(dbBuchAutor.AutorId);
                if (autorname != null)
                {

                    vmBuchDetail.Autoren.Add(autorname);
                }
            }
        }

        //Bildpfade erweitern
        private static void MappeBuchZuBuchVM(BuchVM vmBuch, Buch dbBuch)
        {
            //Ergebnisse von dbBuch in vmBuch mappen
            vmBuch.BuchId = dbBuch.BuchId;
            vmBuch.Anzahl = dbBuch.Anzahl;
            vmBuch.Ausgabe = dbBuch.Ausgabe;            
            vmBuch.Bildpfad = dbBuch.Bildpfad.BildPfadAufbereitung();
            vmBuch.BildAltText = dbBuch.BildAltText.BildTextAufbereitung();
            vmBuch.BuchBeschreibung = dbBuch.BuchBeschreibung;
            vmBuch.Erscheinungsjahr = dbBuch.Erscheinungsjahr;
            vmBuch.ISBN = BuchManager.FormatiereISBN(dbBuch.ISBN);
            vmBuch.KatBezeichnung = dbBuch.Kategorie.KatBezeichnung;
            vmBuch.PreisProTagBrutto = Convert.ToDouble(dbBuch.PreisProTagBrutto);
            vmBuch.Titel = dbBuch.Titel;
            vmBuch.Verfügbarkeit = dbBuch.Verfügbarkeit;
            /*TODO - Logik für Reservierung / ReservierungsManager bauen
           if (dbBuch.Verfügbarkeit <0)
           {
               vmBuchDetail.VerfügbarAb = dbBuch.Reservierungszeile.verliehenbis + 1;
           }
           *
           */
            vmBuch.VerfügbarAb = DateTime.Today.ToShortDateString();
            vmBuch.VerlagBezeichnung = dbBuch.Verlag.VerlagBezeichnung;
            vmBuch.VerleihHäufigkeit = dbBuch.VerleihHäufigkeit;
            vmBuch.Autoren = new List<string>();

            foreach (BuchAutor dbBuchAutor in dbBuch.BuchAutor)
            {
                string autorname = BuchManager.LadeAutorname(dbBuchAutor.AutorId);
                if (autorname != null)
                {

                    vmBuch.Autoren.Add(autorname);
                }
            }
        }
        #endregion
    }
}
