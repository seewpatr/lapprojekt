﻿using Buchverleih.DAL.Data;
using Buchverleih.DAL.Logic;
using Buchverleih.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Buchverleih.Controllers
{
    //TODO - Kunden-Edit => Rechnungsadresse anpassen
    //TODO - Passwort-Wiederherstellung
    //TODO - Toaster ansehen
    //TODO - Statistiken fertig schreiben
    //TODO - Ordner und Dateien im VS aktualisieren
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            StatistikVMListen vmStatistikListen = new StatistikVMListen();
            vmStatistikListen.TopNeu = new List<StatistikVM>();
            vmStatistikListen.TopVerleih = new List<StatistikVM>();
            vmStatistikListen.TopPreis = new List<StatistikVM>();

            foreach (Buch item in StatistikManager.LadeTopBuecher("erscheinung", 10))
            {                   
                vmStatistikListen.TopNeu.Add(MappeBuchZuStatistikVM(item));                
            }

            foreach (Buch item in StatistikManager.LadeTopBuecher("beliebtheit", 10))
            {               
                vmStatistikListen.TopVerleih.Add(MappeBuchZuStatistikVM(item));
            }

            foreach (Buch item in StatistikManager.LadeTopBuecher("preis", 10))
            {               
                vmStatistikListen.TopPreis.Add(MappeBuchZuStatistikVM(item));
            }

            return View(vmStatistikListen);
        }

        private static StatistikVM MappeBuchZuStatistikVM(Buch item)
        {
            StatistikVM vmStatistik = new StatistikVM
            {
                BuchId = item.BuchId,
                Bildpfad = item.Bildpfad,
                BildAltText = item.BildAltText,
                BuchBeschreibung = item.BuchBeschreibung,
                KategorieId = item.KategorieId,
                Titel = item.Titel,
                Verfügbarkeit = item.Verfügbarkeit
            };

            return vmStatistik;
        }

        public ActionResult Eingeloggt()
        {
            ViewBag.Message = "Erfolgreich Eingeloggt";

            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}