﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Buchverleih.Controllers
{
    public class WarenkorbController : Controller
    {
        [HttpGet]
        [Authorize]
        public ActionResult hinzu(int buchid, short anzahl = 1)
        {
            //TODO - Validierung der Daten

            //TODO - Buch dem Einkaufswagen hinzufügen
           // WarenkorbManager.BuchInWarenkorb(buchid, anzahl, User.Identity.Name);

            return RedirectToAction("Buchliste", "Buch");
        }
    }
}