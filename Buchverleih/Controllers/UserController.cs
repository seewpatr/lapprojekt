﻿using Buchverleih.DAL.Logic;
using Buchverleih.Models;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Buchverleih.Controllers
{
    public class UserController : Controller
    {
        //WICHTIG - Methoden mit technischer Dokumentation versehen        

        //TODO - OAuth einbauen

        [HttpGet]
        public ActionResult Registrieren()
        {
            return View();
        }

        //[HttpGet]
        //public ActionResult Registrieren(string x)
        //{
        //    return View();
        //}

        [HttpPost]
        public ActionResult Registrieren(RegistrierenVM vmRegistrieren)
        {
            /* # # # # # Datenvalidierung # # # # #
             * Im Fehlerfall Benutzer zur Seite zurückleiten
             * Fehlermeldung in diesem Fall automatisch über DataAnnotations
             * Simple Variante über ViewBag
             */
            if (!this.ModelState.IsValid)
            {
                return View(vmRegistrieren);
            }

            if (UserManager.ExistiertKunde(vmRegistrieren.Email))
            {
                ViewBag.Fehler = "Ein Benutzer mit dieser E-Mail existiert bereits. ";

                return View(vmRegistrieren);
            }

            //Daten in DB speichern
            UserManager.KundeAnlegen(vmRegistrieren.Email, vmRegistrieren.GeschlechtKurz, vmRegistrieren.Vorname, vmRegistrieren.Nachname, vmRegistrieren.Passwort, vmRegistrieren.Strasse, vmRegistrieren.Plz, vmRegistrieren.Ort);
            //Im Fehlerfall: Benutzer zur Seite zurückleiten    

            //Weiterleitung           
            return RedirectToAction("Erfolgreich", "Home");
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginVM vmLogin)
        {
            //Datenvalidierung, wenn Daten den DataAnnotations entsprechen
            if (!ModelState.IsValid)
            {
                return View(vmLogin);
            }

            //Prüfen ob der Benutzer existiert            

            RegistrierenVM vmRegistrieren = (RegistrierenVM)Session["Benutzer"];
            if (!UserManager.ExistiertKunde(vmLogin.Email))
            {
                ViewBag.Nachricht = "Ein Benutzer mit dieser E-Mail existiert nicht. ";
                return View(vmLogin);
            }


            // Prüfen ob Daten mit Daten in DB übereinstimmen
            if (!UserManager.IstLoginGueltig(vmLogin.Email, vmLogin.Passwort))
            {
                ViewBag.Nachricht = "Zugangsdaten stimmen nicht.";
                return View(vmLogin);
            }

            //Benutzer im System einloggen
            BenutzerAuthentifizieren(vmLogin.Email, false, "");

            ViewBag.Nachricht = "Erfolgreich eingeloggt";
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Logout()
        {
            //Prüfen ob Warenkorb leer - Warenkorb behalten oder leeren

            //Session löschen

            //Auth-Ticket - Erklärt das Ticket für ungültig
            FormsAuthentication.SignOut();

            //Weiterleitung zu Startseite
            //TODO - Message für erfolgreichen Logout mitgeben
            //WICHTIG - Sichtbarkeiten der Buttons einstellen
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Erzeugt ein Cookie mit einem Authentifizierungsticket für einen eingeloggten Benutzer
        /// </summary>
        /// <param name="email">Erwartet die eindeutige E-Mail Adresse des Benutzers vom Datentyp String</param>
        /// <param name="speichereMich">Erwartet einen Boolean, der bestimmt, ob das Cookie das Browser Schließen überlebt</param>
        /// <param name="rollen">Erwartet die Rolle(n) die der Benutzer besitzt</param>
        private void BenutzerAuthentifizieren(string email, bool speichereMich, string rollen)
        {
            /* Erstellt ein Authorisierungsticket
             * 1.   Gibt die Versionsnummer an
             * 2.   Gibt das Ausstellungsdatum an
             * 3.   Gibt die Ablaufzeit an
             * 4.   Gibt an ob das Cookie das Schließen des Browsers überlebt
             * 5.   Gibt die Rolle(n) an die der Benutzer besitzt
             */

            //1. Ticket anlegen
            var authTicket = new FormsAuthenticationTicket(
                1,
                email,
                DateTime.Now,
                DateTime.Now.AddMinutes(30),
                speichereMich,
                rollen
                );

            //2. Ticket verschlüsseln
            string verschluesseltesTicket = FormsAuthentication.Encrypt(authTicket);

            //3. Cookie mit erzeugen das dieses Ticket beinhält
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, verschluesseltesTicket);

            //4. Cookie zur allgemeinen Cookieliste hinzufügen
            System.Web.HttpContext.Current.Response.Cookies.Add(authCookie);
        }
    }
}