  /* ======================================== Checkbox - Filter ======================================== */
    var filter = false;

    function onCheckBoxChange() {
        filter = !filter;
        filterResult();
    }

    /* ======================================== Volltext - Filter ======================================== */
    function filterResult() {
        var table, tr, i;
        table = document.getElementById("buchliste");
        tr = table.getElementsByTagName("tr");

        for (i = 1; i < tr.length; i++) {
            var td = tr[i].getElementsByTagName("td");

            if (!filter) {
                if (
                    td[0] && td[1].innerHTML.toUpperCase().indexOf(document.getElementById("katInput").value.toUpperCase()) > -1 &&
                    td[1] && td[2].innerHTML.toUpperCase().indexOf(document.getElementById("titelInput").value.toUpperCase()) > -1 &&
                    td[2] && td[3].innerHTML.toUpperCase().indexOf(document.getElementById("autorInput").value.toUpperCase()) > -1 &&
                    td[3] && td[4].innerHTML.toUpperCase().indexOf(document.getElementById("isbnInput").value.toUpperCase()) > -1 &&
                    td[4] && td[5].innerHTML.toUpperCase().indexOf(document.getElementById("buchBeschreibungInput").value.toUpperCase()) > -1
                ) {
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }
            }
            else {
                if (
                    td[0] && td[1].innerHTML.toUpperCase().indexOf(document.getElementById("katInput").value.toUpperCase()) > -1 &&
                    td[1] && td[2].innerHTML.toUpperCase().indexOf(document.getElementById("titelInput").value.toUpperCase()) > -1 &&
                    td[2] && td[3].innerHTML.toUpperCase().indexOf(document.getElementById("autorInput").value.toUpperCase()) > -1 &&
                    td[3] && td[4].innerHTML.toUpperCase().indexOf(document.getElementById("isbnInput").value.toUpperCase()) > -1 &&
                    td[4] && td[5].innerHTML.toUpperCase().indexOf(document.getElementById("buchBeschreibungInput").value.toUpperCase()) > -1 &&
                    parseInt(td[8].innerText) > 0
                ) {
                    tr[i].style.display = "";
                }
                else {
                    tr[i].style.display = "none";
                }
            }
        }
    }