﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Buchverleih.Hilfsklassen
{
    public static class StringErweiterungen
    {
            /// <summary>
            /// Überprüft den übergebenenen Bildpfad und setzt einen Standardwert, wenn dieser leer ist
            /// </summary>
            /// <param name="bildpfad">Erwartet einen Bildpfad vom Datentyp String</param>
            /// <returns>Gibt einen Bildpfad vom Datentyp String zurück</returns>
        public static string BildPfadAufbereitung(this string bildpfad)
        {
            if (string.IsNullOrEmpty(bildpfad))
            {
                return $"/Images/Buecher/";
            }
            return bildpfad;
        }

        /// <summary>
        /// Überprüft den übergebenenen Bild-Alternativtext und setzt einen Standardwert, wenn dieser leer ist
        /// </summary>
        /// <param name="bildAltText">Erwartet einen Bild-Alternativtext vom Datentyp String</param>
        /// <returns>Gibt einen Bild-Alternativtext vom Datentyp String zurück</returns>
        public static string BildTextAufbereitung(this string bildAltText)
        {
            if (string.IsNullOrEmpty(bildAltText))
            {
                return "Bild kann nicht angezeigt werden";
            }
            return bildAltText;
        }
    }
}