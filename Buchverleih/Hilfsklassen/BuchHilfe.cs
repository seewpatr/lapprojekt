﻿namespace Buchverleih.Hilfsklassen
{
    public static class BuchHilfe
    {
        /// <summary>
        /// Überprüft den übergebenenen Bildpfad und setzt einen Standardwert, wenn dieser leer ist
        /// </summary>
        /// <param name="bildpfad">Erwartet einen Bildpfad vom Datentyp String</param>
        /// <returns>Gibt einen Bildpfad vom Datentyp String zurück</returns>
        public static string BildPfadAufbereitung(string bildpfad)
        {

            if (string.IsNullOrEmpty(bildpfad))
            {
                return "Standard - Bildpfad";

            }
            return bildpfad;
        }

        /// <summary>
        /// Überprüft den übergebenenen Bild-Alternativtext und setzt einen Standardwert, wenn dieser leer ist
        /// </summary>
        /// <param name="bildAltText">Erwartet einen Bild-Alternativtext vom Datentyp String</param>
        /// <returns>Gibt einen Bild-Alternativtext vom Datentyp String zurück</returns>
        public static string BildTextAufbereitung(string bildAltText)
        {
            if (string.IsNullOrEmpty(bildAltText))
            {
                return "Bild kann nicht angezeigt werden";
            }
            return bildAltText;
        }
    }
}