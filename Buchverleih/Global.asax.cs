﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace Buchverleih
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);            
        }

        /// <summary>
        /// Vergleich ein HttpCookie mit dem Benutzercookie und authentifiziert diesen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            //1. Cookie abrufen und auf Existenz prüfen
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
                            
                if (authCookie == null || authCookie.Value == "")
                {
                    return; 
                }

            //2. Cookie Inhalt (=Auth Ticket) Daten entschlüsseln
            try
            {
                FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);

                //Rollemanagement

                //3. Identität erzeugen
                GenericIdentity authIdentity = new GenericIdentity(authTicket.Name);
                GenericPrincipal authPrincipal = new GenericPrincipal(authIdentity, null);

                //Identität unserer Webanwendung mitteilen
                Context.User = authPrincipal;
            }
            catch (Exception)
            {

                throw;
            }

            //3. Auth-Ticket analysieren

            //4. Identität anlegen
        }
    }
}
