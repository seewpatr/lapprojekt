﻿using Buchverleih.DAL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Buchverleih.DAL.Logic
{
    public static class StatistikManager
    {
        public static List<Buch> LadeTopBuecher(string priorität, byte top)
        {
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                List<Buch> topBuecherListe = new List<Buch>();
                switch (priorität)
                {
                    case "beliebtheit" :
                        foreach (var item in db.Usp_TopBuecherNachBeliebtheit(top))
                        {
                            Buch buch = MappeVonTopSpZuBuch(item);
                            topBuecherListe.Add(buch);
                        }
                        break;
                    case "erscheinung":
                        foreach (var item in db.Usp_TopBuecherNachErscheinung(top))
                        {
                            Buch buch = MappeVonTopSpZuBuch(item);
                            topBuecherListe.Add(buch);
                        }
                        break;
                    case "preis":
                        foreach (var item in db.Usp_TopBuecherNachPreis(top))
                        {
                            Buch buch = MappeVonTopSpZuBuch(item);
                            topBuecherListe.Add(buch);
                        }
                        break;
                    default:
                        throw new Exception("Priorität nicht gefunden");                      
                }
                return topBuecherListe;
            }
        }
       
        private static Buch MappeVonTopSpZuBuch(Usp_TopBuecherNachBeliebtheit_Result item)
        {
            return new Buch()
            {
                BuchId = item.BuchId,
                ISBN = item.ISBN,
                KategorieId = item.KategorieId,

                VerlagId = item.VerlagId,
                Titel = item.Titel,
                Ausgabe = item.Ausgabe,
                Erscheinungsjahr = item.Erscheinungsjahr,
                Anzahl = item.Anzahl,
                PreisProTagNetto = item.PreisProTagNetto,
                PreisProTagBrutto = item.PreisProTagBrutto,
                Verfügbarkeit = item.Verfügbarkeit,
                VerleihHäufigkeit = item.VerleihHäufigkeit,
                BuchBeschreibung = item.BuchBeschreibung,
                Bildpfad = item.Bildpfad,
                BildAltText = item.Bildpfad
            };
        }
        private static Buch MappeVonTopSpZuBuch(Usp_TopBuecherNachPreis_Result item)
        {
            return new Buch()
            {
                BuchId = item.BuchId,
                ISBN = item.ISBN,
                KategorieId = item.KategorieId,
                VerlagId = item.VerlagId,
                Titel = item.Titel,
                Ausgabe = item.Ausgabe,
                Erscheinungsjahr = item.Erscheinungsjahr,
                Anzahl = item.Anzahl,
                PreisProTagNetto = item.PreisProTagNetto,
                PreisProTagBrutto = item.PreisProTagBrutto,
                Verfügbarkeit = item.Verfügbarkeit,
                VerleihHäufigkeit = item.VerleihHäufigkeit,
                BuchBeschreibung = item.BuchBeschreibung,
                Bildpfad = item.Bildpfad,
                BildAltText = item.Bildpfad
            };
        }
        private static Buch MappeVonTopSpZuBuch(Usp_TopBuecherNachErscheinung_Result item)
        {
            return new Buch()
            {
                BuchId = item.BuchId,
                ISBN = item.ISBN,
                KategorieId = item.KategorieId,                
                VerlagId = item.VerlagId,
                Titel = item.Titel,
                Ausgabe = item.Ausgabe,
                Erscheinungsjahr = item.Erscheinungsjahr,
                Anzahl = item.Anzahl,
                PreisProTagNetto = item.PreisProTagNetto,
                PreisProTagBrutto = item.PreisProTagBrutto,
                Verfügbarkeit = item.Verfügbarkeit,
                VerleihHäufigkeit = item.VerleihHäufigkeit,
                BuchBeschreibung = item.BuchBeschreibung,
                Bildpfad = item.Bildpfad,
                BildAltText = item.Bildpfad
            };
        }

    }
}