﻿using Buchverleih.DAL.Data;
using System.Linq;

namespace Buchverleih.DAL.Logic
{
    public static class WarenkorbManager
    {
        //TODO - Technische Doku!!!
        public static bool BuchInWarenKorb(int buchId, short anzahl, string email)
        {
            var dbBuch = BuchManager.LadeBuch(buchId);
            var dbKunde = UserManager.LadeKundeVonEmail(email);

            //Überprüfungen
            //Ist die Anzahl vernünftig? (postiv & >0)
            if (anzahl <=0)
            {
                return false;
            }

            //Gibt es das Buch noch?
            if (dbBuch == null)
            {
                //Wenn nein, return false
                return false;
            }

            //Sind genügend Exemplare verfügbar?
            //Gibt es den Benutzer
            if ( dbKunde == null)
            {
                //Wenn nein, return false
                return false;
            }

            //Überprüfen ob Buch verfügbar
            if (anzahl > dbBuch.Verfügbarkeit)
            {
                //Wenn nein, return false
                return false;
            }

            //Wenn verfügbar, prüfen ob Benutzer schon einen WK hat
            var warenkorbId = WarenkorbManager.LadeWarenkorbIdVonKunde(dbKunde.KundenId);
            if (warenkorbId == -1) // -1 heißt, dass es keinen Warenkorb gibt 
            {
                //Wenn nein => WK anlegen und weiter
                warenkorbId = WarenkorbManager.NeuerWarenkorb(dbKunde.KundenId);
            }

            //Wenn ja   => prüfen ob das Buch schon einmal im WK liegt
            if (WarenkorbManager.LiegtSchonImWarenkorb(buchId, warenkorbId))
            {
                //Wenn ja   => Anzahl erhöhen
              //  WarenkorbManager.ErhoeheAnzahl(buchId, dbWarenkorbId, anzahl);
            }
            else
            {
                //Wenn nein => Eintrag anlegen
              //  WarenkorbManager.NeueReservierungszeile(buchId, warenkorbId, anzahl);
            }

            //Wenn alles gepasst hat, return true
            return true;
        }

        private static int LadeWarenkorbIdVonKunde(int kundeId)
        {
            //Lade alle Reservierungen des Kunden WHERE VerliehenVon / VerliehenBis == null
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                Reservierung dbReservierung = db.Reservierungen.Where(r => r.KundenId == kundeId && r.VerliehenBis == null && r.VerliehenVon == null).FirstOrDefault();

                if (dbReservierung == null)
                {
                    return -1;
                }
                return dbReservierung.ReservierungId;
            }            
        }

        private static int NeuerWarenkorb(int kundeId)
        {
            //TODO - Nochmals prüfen ob der Warenkorb schon existiert

            Reservierung dbReservierung = new Reservierung();

            dbReservierung.KundenId = kundeId;
            dbReservierung.VerliehenBis = null;
            dbReservierung.VerliehenVon = null;
            dbReservierung.GesamtBrutto = 0;
            dbReservierung.GesamtBruttoRabattiert = 0;
            dbReservierung.GesamtNetto = 0;
            dbReservierung.GesamtNettoRabattiert = 0;

            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                db.Reservierungen.Add(dbReservierung);
                if (db.SaveChanges() !=1)
                {
                    //Bei SaveChanges sollte der Wert 1 (1 DS betroffen) zurück gegeben werden
                    return -1;
                }
                return dbReservierung.ReservierungId;
            }           
        }

        private static bool LiegtSchonImWarenkorb(int buchId, int warenkorbId)
        {
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                return db.Reservierungszeilen.Any(b => b.BuchId == buchId && b.ReservierungId == warenkorbId);
            }
        }
    }
}