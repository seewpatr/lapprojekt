﻿using Buchverleih.DAL.Data;
using System;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;

namespace Buchverleih.DAL.Logic
{
    public class UserManager
    {
        #region Main-Methoden
        /// <summary>
        /// Prüft anhand der E-Mail Adresse ob ein Kunde bereits in der Datenbank existiert
        /// </summary>
        /// <param name="email">Nimmt als Parameter die E-Mail des Kunden an</param>
        /// <param name="meldung">Gibt eine Statusmeldung vom Datentyp string zurück</param>
        /// <returns>Wenn der Kunden bereits existiert wird true, ansonsten false zurückgegeben</returns>
        public static bool ExistiertKunde(string email)
        {
            //Suche Kunden mit übergebener Email in der Datenbank
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                return db.Kunden.Any(x => x.Email == email);
            }
        }

        /// <summary>
        /// Legt einen neuen Kunde in der Datenbank an
        /// </summary>
        public static void KundeAnlegen(

            string email,
            char geschlecht,
            string kundenVorname,
            string kundenNachname,
            string klarTextPasswort,
            string strasse,
            string plz,
            string ort
            )
        {
            // ======================================== Passwort hashen ========================================            

            //1. Salt erzeugen
            byte[] salt = SaltErzeugen();

            //2. Gesaltetes Passwort erzeugen
            StringBuilder gesaltetesPasswort = new StringBuilder();
            gesaltetesPasswort.Append($"{klarTextPasswort}{ErzeugeHexStringAusBytes(salt)}");

            //3. Gesaltetes Passwort hashen
            byte[] gesaltetesPasswortUndGehashtesPasswort = HashAusStringErzeugen(gesaltetesPasswort.ToString());

            //4. Daten in die DB speichern (Achtung! Statt PW das gesaltete PW verwenden!)
            {
                //a. Neues Kundenobjekt anlegen
                var dbKundeNeu = new Kunde()
                {
                    Email = email,
                    Geschlecht = geschlecht.ToString(),
                    KundenVorname = kundenVorname,
                    KundenNachname = kundenNachname,
                    Strasse = strasse,
                    PLZ = plz,
                    Ort = ort,
                    Salt = salt,
                    Passwort = gesaltetesPasswortUndGehashtesPasswort
                };

                //b. Verbindung zur DB aufbauen und Kunden abspeichern
                using (BuchverleihEntities db = new BuchverleihEntities())
                {
                    if (ExistiertKunde(dbKundeNeu.Email))
                    {
                        throw new Exception("Email bereits vergeben");
                    }

                    db.usp_BenutzerAnlegen(dbKundeNeu.Email, dbKundeNeu.Geschlecht.ToString(), dbKundeNeu.KundenNachname, dbKundeNeu.KundenVorname, gesaltetesPasswortUndGehashtesPasswort, salt, dbKundeNeu.Strasse, dbKundeNeu.PLZ, dbKundeNeu.Ort);

                    db.SaveChanges();
                }
            }
        }
       
        /// <summary>
        /// Prüft ob das vom Benutzer eingegebene Passwort mit dem in der DB gespeicherten Passwort übereinstimmt
        /// </summary>
        /// <param name="vmLogin">Nimmt ein ViewModel vom Typ LoginVM entgegen</param>
        /// <returns>Wenn der Login gültig ist wird true, ansonsten false zurückgegeben</returns>
        public static bool IstLoginGueltig(string email, string passwort)
        {
            //Kunde aus DB laden und überprüfen ob Kunde existiert
            Kunde dbKunde = LadeKundeVonEmail(email);
            if (dbKunde == null)
            {
                return false;
            }

            // ======================================== Passwörter vergleichen ========================================
            // 1. Salt aus DB laden
            byte[] dbSalt = dbKunde.Salt;

            // 2. Hash aus DB laden
            byte[] dbHash = dbKunde.Passwort;

            // 3. Das eingegebene Passwort mit dem Salt kombinieren
            StringBuilder gesaltetesEingabePW = new StringBuilder();
            gesaltetesEingabePW.Append($"{passwort}{ErzeugeHexStringAusBytes(dbSalt)}");

            // 4. Das gesaltete Passwort hashen
            byte[] hashEingabePW = HashAusStringErzeugen(gesaltetesEingabePW.ToString());

            // 5. Den Hash aus der Eingabe mit dem Hash aus der DB vergleichen
            //      Wenn gleich return true, ansonsten false
            return (hashEingabePW.SequenceEqual(dbHash));
        }
        /// <summary>
        /// Sucht einen Kunden anhand seiner E-Mail Adresse und gibt diesen zurück
        /// </summary>
        /// <param name="email">Erwartet die E-Mail Adresse des Kunden vom Datentyp String</param>
        /// <returns>Gibt einen Kunden vom Datentyp Kunde zurück</returns>
        public static Kunde LadeKundeVonEmail(string email)
        {
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                return db.Kunden.Where(x => x.Email == email).FirstOrDefault();
            }
        }
        #endregion

        #region Hilfsmethoden
        /// <summary>
        /// Erzeugt durch kryptografische Methoden einen zufälligen Salt
        /// </summary>
        /// <returns>Gibt einen zufällig erzeugten Salt als byte Array zurück</returns>
        public static byte[] SaltErzeugen()
        {
            byte[] salt = new byte[64];

            //Ist eine kryptographische Zufallserzeugung
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();

            //Diese Methode befüllt eine Variable mit einer Zufallszahl an Bytes, die keine 0 enthalten
            rng.GetNonZeroBytes(salt);

            return salt;
        }

        /// <summary>
        /// Nimmt ein byte Array entgegen und erzeugt einen String aus Hexadezimalzahlen
        /// </summary>
        /// <param name="bytes">Nimmt in byte Array als Parameter entgegen</param>
        /// <returns>Gibt einen String aus Hexadezimalzahlen zurück</returns>
        public static string ErzeugeHexStringAusBytes(byte[] bytes)
        {
            StringBuilder stringBytes = new StringBuilder();

            //X2 gibt an, das der String in eine Hexadezimalzahl umgewandelt wird            
            foreach (byte b in bytes)
            {
                stringBytes.Append(b.ToString("X2"));
            }

            return stringBytes.ToString();
        }

        /// <summary>
        /// Nimmt einen String entgegen und erzeugt ein einen Hash, der als byte Array zurückgegeben wird
        /// </summary>
        /// <param name="str">Nimmt einen String entgegen</param>
        /// <returns>Gibt einen Hash als byte Array zurück</returns>
        private static byte[] HashAusStringErzeugen(string str)
        {
            //Umwandlung aus dem HexString in einen BytesString
            byte[] stringBytes = Encoding.UTF8.GetBytes(str);

            using (SHA512Managed hasher = new SHA512Managed())
            {
                //Wandelt den übergebenen String in einen Hash um
                byte[] hash = hasher.ComputeHash(stringBytes);
                return hash;
            }
        }

        //TODO - E-Mail Versand konfigurieren (ev. ChronJob bauen)
        /// <summary>
        /// Verschickt eine E-Mail an die entsprechenden Daten
        /// </summary>
        private static void EmailSchicken()
        {
            string datei = "D:\\ssl_smtp.txt";

            MailMessage message = new MailMessage();
            //Absender
            message.To.Add(new MailAddress("patleth@gmx.at", "meineWebsite.tk"));

            //Empfänger
            message.From = new MailAddress("patrick.seewald@qualifizierung.at");

            //Kopie - Empfänger
            message.CC.Add(new MailAddress("MyEmailID@gmail.com"));

            //Betreff
            message.Subject = "Using the new SMTP client.";

            //Inhalt
            message.Body = @"Using this new feature, you can send an email message from an application very easily.";

            //Prüfen ob Datei null ist
            if (datei != null)

                //Dateianhang
                message.Attachments.Add(new Attachment(datei));

            SmtpClient client = new SmtpClient("srv009.edu.local", 587);
            client.Credentials = new System.Net.NetworkCredential("seewpatr@edu.local", "Cr203210");
            client.EnableSsl = true;
            client.Send(message);
        }
        #endregion

    }
}