﻿using Buchverleih.DAL.Data;
using Buchverleih.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Buchverleih.DAL.Logic
{
    public static class BuchManager
    { 
        /// <summary>
        /// Ladet eine List von Büchern vom Typ Buch aus der Datenbank
        /// </summary>
        /// <returns>Gibt eine List vom Datentyp BuchVM zurück</returns>
        public static List<Buch> LadeBuecher()
        {
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                return db.Buecher
                    .Include(b => b.Kategorie)
                    .Include(b => b.Verlag)
                    .Include(b => b.BuchAutor)
                    .ToList();
            }           
        }

        //TODO - JOIN mit Schlüsselwort join bauen

        /// <summary>
        /// Ladet ein Buch anhand der BuchId aus der Datenbank
        /// </summary>
        /// <param name="buchId">Erwartet eine BuchId vom Datentyp Integer</param>
        /// <returns>Gibt ein Buch vom Datentyp Buch zurück</returns>
        public static Buch LadeBuch(int buchId)
        {
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                //Join mit LINQ
                return db.Buecher
                    .Include(b => b.Kategorie)
                    .Include(b => b.Verlag)
                    .Include(b => b.BuchAutor)
                    .Where(b => buchId == b.BuchId)
                    .FirstOrDefault();

            }
        }

        #region Hilfsmethoden
        /// <summary>
        /// Formatiert einen ISBN und trennt diesen mit Hilfe von Trennzeichen (Bindestrichen) auf
        /// </summary>
        /// <param name="buch">Erwartet ein Buch vom Datentyp Buch</param>
        /// <returns>Gibt einen formatierten ISBN in Form eines String zurück</returns>
        public static string FormatiereISBN(string isbn)
        {
            StringBuilder formatierteISBN = new StringBuilder();
            formatierteISBN.Append($"{isbn.Substring(0, 3)}-{isbn.Substring(3, 1)}-{isbn.Substring(4, 4)}-{isbn.Substring(8, 4)}-{isbn.Substring(12, 1)}");

            return formatierteISBN.ToString();
        }

        /// <summary>
        /// Wandelt (Mappt) einen Autor vom Datentyp Autor in den Datentyp AutorVM
        /// </summary>
        /// <param name="autorId">Erwartet eine autorId vom Datentyp integer</param>
        /// <returns>Gibt einen AutorVM zurück</returns>
        public static string LadeAutorname(int autorId)
        {
            using (BuchverleihEntities db = new BuchverleihEntities())
            {
                var dbAutor = db.Autoren.Find(autorId);
                if (dbAutor == null)
                {
                    return null;
                }
                return $"{dbAutor.AutorVorname} {dbAutor.AutorNachname}";
            }
        }
        #endregion
    }
}