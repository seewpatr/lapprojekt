﻿using Buchverleih.DAL.Data;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Buchverleih.Models
{
    public enum Geschlecht
    {
        Neutral, Mann, Frau
    }
    #region Eigene Data-Annotations
    /// <summary>
    /// Diese Klasse stellt eine benutzerdefinierte Validierung dar, die prüft, ob eine Checkbox angehakt (true gesetzt) ist
    /// </summary>
    public class Akzeptiert : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool)value;
        }
    }

    /// <summary>
    /// Diese Klasse stellt eine benutzerdefinierte Validierung dar, die prüft, ob eine eingegebene E-Mail Adresse bereits in der Datenbank existiert
    /// </summary>
    public class ExistiertEmail : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //Temporärer Benutzer
            Kunde tempKunde;

            using (var db = new BuchverleihEntities())
            {
                //Suche Kunden mit übergebener Email in der Datenbank
                tempKunde = db.Kunden.FirstOrDefault(x => x.Email == value.ToString());
                //Wenn Kunde existiert => true
                if (tempKunde != null)
                {
                    var errorMessage = FormatErrorMessage(validationContext.DisplayName);
                    return new ValidationResult(errorMessage);
                }
                //Wenn Kunde nicht existiert => false
                return ValidationResult.Success;
            }
        }
    }

    #endregion

    public class RegistrierenVM
    {
        /*         
         *  ======================================== DataAnnotations ========================================
         *      Required        Legt fest, dass das Feld befüllt werden muss
         *      DataType        Legt den Datentyp des Feldes fest
         *          EmailAdress         Legt das Format der Emailadresse fest
         *          Password            Verschleiert das Eingabefeld
         *          RegularExpression   Legt eine bestimmte Formatierung fest
         *          Compare             Vergleicht mit dem übergebenen Feld    
         *          ErrorMessage        Übergibt eine passende ErrorMessage
         *      Akzeptiert              Eine selbst erstellte Data Annotation um eine Checkbox zu prüfen
         *      ExistiertEmail          Eine selbst erstellte Data Annotation um eine bereits registrierte E-Mail Adresse zu prüfen
         */
        [Required]         
        public Geschlecht Geschlecht { get; set; }

        public char GeschlechtKurz
        {
            get
            {
                switch (Geschlecht)
                {
                    case Geschlecht.Frau:
                        return 'w';
                    case Geschlecht.Neutral:
                        return 'n';
                    case Geschlecht.Mann:
                        return 'a';
                    default:
                        throw new Exception("Das sollte nicht passieren");
                }
            }
        }

        [Required(ErrorMessage = "Bitte geben Sie Ihren Vornamen ein.")]
        public string Vorname { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihren Nachnamen ein.")]
        public string Nachname { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihre Wohnadresse ein.")]
        public string Strasse { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihren Wohnort ein.")]
        public string Ort { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihre Postleitzahl ein.")]
        [RegularExpression("^[1-9][0-9]{3}$", ErrorMessage = "Dies ist keine gültige Postleitzahl.")]
        public string Plz { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihre E-Mail Adresse ein.")]
        [EmailAddress(ErrorMessage = "Dies ist keine gültige E-Mail Adresse.")]
        [ExistiertEmail(ErrorMessage = "Diese E-Mail Adresse ist bereits vergeben.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihr Passwort ein.")]
        [DataType(DataType.Password)]
        [RegularExpression("^[A-Za-zßẞ0-9!§$%&()=?@€*+~#<>]{8,50}$", ErrorMessage = "Das Passwort muss mind. 8 Zeichen lang sein, Groß- und Kleinbuchstaben, mind. 1 Zahl sowie mind. 1 Sonderzeichen enthalten.")]
        public string Passwort { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihr Passwort erneut ein.")]
        [DataType(DataType.Password)]
        [Compare("Passwort", ErrorMessage = "Die beiden Passwörter stimmen nicht überein.")]
        public string PasswortWH { get; set; }

        //[Required]
        [Akzeptiert(ErrorMessage = "Sie müssen die AGB lesen und bestätigen.")]
        //[Range(typeof(bool), "true", "true", ErrorMessage = "Sie müssen die AGB lesen und bestätigen.")]
        public bool AgbGelesen { get; set; }
    }
}