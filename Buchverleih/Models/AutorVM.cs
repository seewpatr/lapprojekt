﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Buchverleih.Models
{
    public class AutorVM
    {
        [Required(ErrorMessage = "Bitte geben einen Vornamen ein.")]        
        public string AutorVorname { get; set; }

        [Required(ErrorMessage = "Bitte geben einen Nachnamen ein.")]
        public string AutorNachname { get; set; }
    }
}