﻿using System.ComponentModel.DataAnnotations;

namespace Buchverleih.Models
{
    public class LoginVM
    {
             
         /*  # # # # # DataAnnotations # # # # #
          *      Required        Legt fest, dass das Feld befüllt werden muss
          *      DataType        Legt den Datentyp des Feldes fest
          *          EmailAdress         Legt das Format der Emailadresse fest
          *          Password            Verschleiert das Eingabefeld                    
          */

        [Required(ErrorMessage = "Bitte geben Sie Ihre E-Mail Adresse ein.")]
        [EmailAddress(ErrorMessage = "Dies ist keine gültige E-Mail Adresse.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie Ihr Passwort ein.")]
        [DataType(DataType.Password)]
        public string Passwort { get; set; }

    }
}