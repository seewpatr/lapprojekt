﻿using System.ComponentModel.DataAnnotations;

using System;
using System.Collections.Generic;

namespace Buchverleih.Models
{ 
    public class StatistikVM
    {
        public int BuchId { get; set; }

        [Required(ErrorMessage = "Bitte geben sie eine gültige KategorieId ein.")]
        public int KategorieId { get; set; }

        public string KatBezeichnung { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen gültigen Titel ein. ")]
        public string Titel { get; set; }

        public short? Verfügbarkeit { get; set; }

        public string BuchBeschreibung { get; set; }

        public string Bildpfad { get; set; }

        public string BildAltText { get; set; }

    }

    public class StatistikVMListen
    {
        public List<StatistikVM> TopVerleih { get; set; }
        public List<StatistikVM> TopNeu { get; set; } 
        public List<StatistikVM> TopPreis { get; set; }
    }
}