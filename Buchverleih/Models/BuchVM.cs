﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Buchverleih.Models
{
    #region Eigene Data-Annotations
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class RangeUntilCurrentYearAttribute : RangeAttribute
    {
        public RangeUntilCurrentYearAttribute(int minimum) : base(minimum, DateTime.Now.Year)
        {
        }
    }
    #endregion

    public class BuchVM
    {
        /*         
         *  ======================================== DataAnnotations ========================================
         *      Required                        Legt fest, dass das Feld befüllt werden muss
         *      DataType                        Legt den Datentyp des Feldes fest
         *      Range                           Legt einen bestimmten Wertebereich fest
         *          RegularExpression           Legt eine bestimmte Formatierung fest
         *          ErrorMessage                Übergibt eine passende ErrorMessage
         *      RangeUntilCurrentYearAttribute  Legt fest, das die Eingabe nach 1900 und vor dem aktuellen Datum sein muss
         */

        public int BuchId { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine gültige ISBN ein.")]
        [RegularExpression("^[1-9]{3}[-][0-9][-][0-9]{4}[-][0-9]{4}[0-9]$", ErrorMessage = "Dies ist keine gültige ISBN.")]
        public string ISBN { get; set; }

        [Required(ErrorMessage = "Bitte geben sie eine gültige KategorieId ein.")]
        public int KategorieId { get; set; }

        public string KatBezeichnung { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie eine gültige VerlagId ein.")]
        public int VerlagId { get; set; }

        public string VerlagBezeichnung { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie einen gültigen Titel ein. ")]
        public string Titel { get; set; }

        [Range(1, 32000, ErrorMessage = "Die Zahl muss zwischen 1 und 32.000 liegen.")]
        public short Ausgabe { get; set; }

        [RangeUntilCurrentYear(1900, ErrorMessage = "Bitte geben Sie ein gültiges Jahr ein.")]
        public short Erscheinungsjahr { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie die verfügbare Anzahl ein.")]
        public short Anzahl { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie den Preis pro Buch (netto) ein. ")]
        [DataType(DataType.Currency, ErrorMessage = "Bitte geben Sie einen gültigen Betrag ein.")]
        public double PreisProTagNetto { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie den Preis pro Buch (brutto) ein. ")]
        [DataType(DataType.Currency, ErrorMessage = "Bitte geben Sie einen gültigen Betrag ein.")]
        public double PreisProTagBrutto { get; set; }

        public short? Verfügbarkeit { get; set; }

        public string VerfügbarAb { get; set; }

        public int VerleihHäufigkeit { get; set; } //TODO - Counter einbauen, der mit den Bestellungen zusammenhängt, trigger

        public string BuchBeschreibung { get; set; }

        public string Bildpfad { get; set; }

        public string BildAltText { get; set; }

        [Required(ErrorMessage = "Bitte geben Sie ein oder mehrere Autoren ein. ")]
        public List<string> Autoren { get; set; }
    }
}